import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { ApiService } from 'src/app/services/api.service';
import { CustomerChatComponent } from 'src/app/views/customer/customer-chat/customer-chat.component';

@Component({
  selector: 'app-startchat',
  templateUrl: './startchat.component.html',
  styleUrls: ['./startchat.component.css']
})
export class StartchatComponent implements OnInit {
  info;
  show_send = false;
  error = {
    show:false,
    message:''
  }
  show_progress = false;
  constructor(private dialog: MatDialog,private api: ApiService ,@Inject(MAT_DIALOG_DATA) private data:any, private dialogRef: MatDialogRef<StartchatComponent>) { }

  ngOnInit() {
    this.info = this.data
  }

  cancel(){
    this.dialogRef.close()
  }

  send(){
    let msg = document.querySelector<any>('.chat-box').value;
    this.error.show = false;

    if(msg != ''){
      this.show_progress = true;
      this.api.sendToSeller(msg, this.info.id).subscribe(
        (data:any)=>{
          this.show_progress = false;

          if(data.success == true){
              this.show_send = true;
              setTimeout(() => {
                this.dialog.open(CustomerChatComponent, {width:'950px', height:'650px'})
              }, 1000);
          }
          if(data.success == false){
            this.error.show = true;
            this.error.message = data.message
          }
        }
      )
    }
  }

}
