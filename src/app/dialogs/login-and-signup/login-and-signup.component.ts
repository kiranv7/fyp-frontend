import { Component, OnInit, Input, Inject } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NavbarComponent } from 'src/app/views/navbar/navbar.component';
import {  MAT_DIALOG_DATA } from '@angular/material';
export interface LoginRes{
  success:boolean,
  first_name:string,
  token:string,
  username:string,
  cart_count:number
}


@Component({
  selector: 'app-login-and-signup',
  templateUrl: './login-and-signup.component.html',
  styleUrls: ['./login-and-signup.component.css']
})
export class LoginAndSignupComponent implements OnInit {
  showLogin:boolean = true;
  loginForm: FormGroup;
  signupForm: FormGroup;
  login_error;
  signup_error;
  showMatProgress:boolean = false;

  constructor(private fb: FormBuilder, private auth: AuthenticationService, @Inject(MAT_DIALOG_DATA) private info:any ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: ['',Validators.required],
      password:['', Validators.required]
    })

    this.signupForm = this.fb.group({
      email: ['', Validators.email],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: ['', Validators.minLength(10)],
      gender: ['', Validators.required],
      password: ['', Validators.minLength(8)],
      cpassword: ['', Validators.minLength(8)]
    })
  }

  get isLoggedIn(){
    return this.auth.getisLoggedin()
  }

  submitLogin(){
    this.auth.clearLocal()
    this.showMatProgress = true;
    this.auth.login(this.loginForm.value).subscribe((data : LoginRes) =>{
      //console.log(data);
      this.login_error = ''
      if(data.success){
        //console.log(data);
        this.auth.setAuth(data.first_name, data.username, data.token, data.success, data.cart_count);
        this.showMatProgress = false
        this.info.navbar.notif_Init()       
      }
    },
    error=>{
      this.login_error = error.error.non_field_errors;
      window.localStorage.setItem("spiologgedIn", "false")
      window.localStorage.removeItem("spiotoken")
      //console.log(error)
      this.showMatProgress = false
    })
  }

  signUp(){
    //console.log(this.signupForm.value);
    this.showMatProgress = true;
    if(this.signupForm.controls['password'].value == this.signupForm.controls['cpassword'].value){
      this.signup_error = '';
      
      this.auth.signUp(this.signupForm.value).subscribe((data:any)=>{
        if(data.success){
          this.auth.setAuth(data.first_name, data.username, data.token, data.success, 0);

          this.showMatProgress = false
        }
        else{
          this.signup_error = data.message
          this.showMatProgress =false
        }
      },
      (error)=>{
        console.log(error);
      })
    }
    else{
        this.signup_error = "Please confirm the password correctly."
        this.showMatProgress = false
    }
  }

}
