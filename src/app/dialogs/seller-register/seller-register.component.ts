import { Component, OnInit } from '@angular/core';
import { Validators, Form, FormBuilder, FormGroup } from '@angular/forms';
import { CartService } from 'src/app/services/cart/cart.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-seller-register',
  templateUrl: './seller-register.component.html',
  styleUrls: ['./seller-register.component.css']
})
export class SellerRegisterComponent implements OnInit {
  signupForm: FormGroup;
  signupForm2: FormGroup;
  signup_error;
  showProgress = false;
  success = false;

  constructor(private fb: FormBuilder, private cart: CartService, private auth: AuthenticationService, private router: Router) { }

  ngOnInit() {
    this.signupForm = this.fb.group({
      email: ['', [Validators.email, Validators.required]],
      seller_name:[''],
      company_name: ['', Validators.required],
      phone: ['', [Validators.minLength(10), Validators.required]],
      password: ['', [Validators.minLength(8), Validators.required]],
      cpassword: ['',[Validators.minLength(8), Validators.required]]
    })

    this.signupForm2 = this.fb.group({
      gstin: ['', [Validators.minLength(15), Validators.required]],
      acc_no:['', [Validators.minLength(12), Validators.required]],
      ifsc_code: ['', [Validators.minLength(4), Validators.required]],
      your_state: ['', Validators.required],
      your_city: ['', Validators.required],
    })
  }

  sellerRegister(){
    if(this.signupForm.value.password == this.signupForm.value.cpassword){
        this.signup_error = ''
        this.showProgress = true
        this.cart.sellerRegister(this.signupForm.value, this.signupForm2.value).subscribe((data:any)=>{
          console.log(data);
          this.showProgress = false
          if(data.success == true){
            this.auth.setSellerLocal(data.seller_name, data.token, data.username, data.success)
            this.router.navigate(['seller/verify'])
            this.success = true;
          }else{
            this.signup_error = data.message
          }
        })
    }
    else{
      this.signup_error = "Confirm the password correctly."
    }
  }


}
