import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-seller-login',
  templateUrl: './seller-login.component.html',
  styleUrls: ['./seller-login.component.css']
})
export class SellerLoginComponent implements OnInit {
  sellerloginForm: FormGroup;
  loginErrors:string;
  showSuccess= false;
  showBusy = false;

  constructor(private fb: FormBuilder, private auth: AuthenticationService, private router:Router) { }

  ngOnInit() {
    this.sellerloginForm = this.fb.group({
      sellerid: ['',Validators.required],
      password:['', Validators.required]
    })
  }

  sellerLogin(){
    this.showBusy = true;
    this.auth.sellerLogin(this.sellerloginForm.value).subscribe((data:any)=>{
      if(data.success == true){
        this.auth.setSellerLocal(data.name, data.token, this.sellerloginForm.value.sellerid, data.success)
        this.showSuccess = true
        this.showBusy = false;
        if(data.verified == false)
        {
          this.router.navigate(['seller/verify'])
        }
        else{
          if(data.admin_approved == false)
            this.router.navigate(['seller/under-review'])
          else
            this.router.navigate(['seller/dashboard'])
        }
      }
      else{
        this.loginErrors = data.message
        this.showBusy = false;
        this.auth.clearSellerLocal()
      }
    })
  }

}
