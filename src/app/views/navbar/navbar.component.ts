import { Component, OnInit,Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { LoginAndSignupComponent } from 'src/app/dialogs/login-and-signup/login-and-signup.component';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { CartService } from 'src/app/services/cart/cart.service';
import { CustomerChatComponent } from '../customer/customer-chat/customer-chat.component';
import { MatSnackBar } from '@angular/material';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, switchMap, distinctUntilChanged } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';

export interface LogotData{
  success:boolean
  message:string
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  conn: WebSocket;
  response: any;
  search_res:any = null;
  closeSocket: Boolean = false;
  chats= null;

  constructor(private snackbar:MatSnackBar, public dialog: MatDialog, private auth: AuthenticationService, private router: Router,private cart: CartService) { }
  isLoggedIn:any; 

  ngOnInit() {
    this.notif_Init()

    const searchBox:any = document.getElementById('search-box');

    searchBox.addEventListener("focusout", ()=>{
      setTimeout( ()=>{
        console.log('focusout')
        this.search_res = null;
      }, 2000)
    })

    const typeahead = fromEvent(searchBox, 'input').pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 2),
      debounceTime(10),
      distinctUntilChanged(),
      switchMap(() => { 
        this.search_res = null;
        return ajax(`http://127.0.0.1:8000/api/search-product/?input=${searchBox.value}`)
      })
    );

    typeahead.subscribe(data => {
      // Handle the data from the API
      if(data.response.success) {
        this.search_res = JSON.parse(data.response.result)
      }
    });

  }

  ngOnChanges(){
    this.notif_Init()
  }

  notif_Init(){
    this.isLoggedIn = false //window.localStorage.getItem('spiologgedIn') ? window.localStorage.getItem('spiologgedIn') : false;
    this.getCartLength

    let token = this.auth.getToken()
    let uid = this.auth.getUserId()

    if(token && uid){
      let req = {
        req_type:"auth",
        token: token,
        username: uid
      }

      this.conn = new WebSocket("ws://127.0.0.1:8000/customer-notification/")

      this.conn.onopen = (e)=>{
        this.conn.send( JSON.stringify(req) )
      }

      this.conn.onmessage = (e)=>{
        this.response = JSON.parse(e.data)
        if(this.response.success == true){
          if(this.response.chats > 0)
            this.chats = this.response.chats
            
          if(this.response.type == "notification"){
            this.snackbar.open(`You have one new message from ${this.response.seller}.`, "OK", {
              duration: 3000,
            })
          }
        }
        else{
          this.closeSocket = true
        }
      }

      this.conn.onclose = (e)=>{
        if(this.closeSocket == false){
          this.notif_Init()
        }
      }
    }
  }
  
  chat(){
    this.dialog.open(CustomerChatComponent, {width:'950px', height:'650px'})
    this.chats = null;
  }

  logoutUser(){
    this.auth.logout().subscribe((data: LogotData)=>{
      if(data.success){
        this.auth.clearLocal();
        this.router.navigate([''])
      }
    })
  }

  loginSignupBtn(){
    this.dialog.open(LoginAndSignupComponent,{width: '650px', data:{navbar:this}})
  }

  get userName(){
    return this.auth.getUsername();
  }

  get getisLoggedin(){
    this.isLoggedIn = this.auth.getisLoggedin()
    return this.isLoggedIn
  }

  get getCartLength(){
    if(this.cart.cartLength == 0){
      return false
    }
    else{
      return this.cart.cartLength
    }
  }


  search() {
    let searchBox:any =  document.getElementById('search-box');
    if( searchBox.value != '' ) {
      this.router.navigate(['search',`${searchBox.value}`])
    }
  }


  goToCart(){
    if (this.auth.getisLoggedin()){
      this.router.navigate(["/cart"])
    }
    else{
      this.dialog.open(LoginAndSignupComponent, {width: '650px', data:{navbar:this}})
    }
  }
}
