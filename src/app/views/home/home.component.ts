import { Component, OnInit } from '@angular/core';
import { element } from 'protractor';
import { DataService } from 'src/app/services/data.service';
import { ApiService } from 'src/app/services/api.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  products:any = [];
  slider;
  isDown = false;
  startX;
  scrollLeft;

  constructor(private api: ApiService, private auth: AuthenticationService) { }

  ngOnInit() {
    this.slider = document.querySelector('.products');
    this.api.getAllProducts().subscribe(data=>{
      this.products = data
      this.products.reverse()
      this.calRealPrice()

      this.auth.checkAuth()
      
    })

  }

  ngOnChanges(){
    this.calRealPrice();
  }

  calRealPrice(){
    //console.log(this.products);
    
    if(typeof this.products != 'undefined' && this.products.length > 0){
      console.log(this.products);
      
      this.products.forEach(element=>{
        if( parseInt(element.discount_percentage) > 0 ){
          element.realprice = parseInt(element.price) - parseInt(element.price) * (parseInt(element.discount_percentage) / 100)
        }
        else{
          element.realprice = parseInt(element.price)
        }
      })
    }
  }

  mouseDown(e){
    this.isDown = true;
    this.slider.classList.add('active');
    this.startX = e.pageX - this.slider.offsetLeft;
    this.scrollLeft = this.slider.scrollLeft;
    console.log('cls');
    
  }

  mouseLeave(){
    this.isDown = false;
    this.slider.classList.remove('active')
  }

  mouseMove(e){
    if(!this.isDown) return;
    e.preventDefault();
    const x = e.pageX - this.slider.offsetLeft;
    const walk = (x - this.startX) * 3; //scroll-fast
    this.slider.scrollLeft = this.scrollLeft - walk;
  }

  mouseUp(){
    this.isDown = false;
    this.slider.classList.remove('active')
  }

}
