import { Component, OnInit, Input } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {
  banners = [];

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    navSpeed: 1000,
    autoplay:true,
    dragEndSpeed:1000,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      940: {
        items: 1
      }
    },
    nav: false
  }

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.api.getBanners()
    .subscribe(data=>{
      this.banners = JSON.parse(data.banners)
      console.log(this.banners)
    })
    
  }

}
