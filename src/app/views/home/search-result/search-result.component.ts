import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ajax } from 'rxjs/ajax';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {
  loaded = false;
  products = [];
  input;
  filter;
  ratings = [];

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params=> {
      this.input = params.input

      fetch(`http://127.0.0.1:8000/api/search-product/?input=${this.input}`)
        .then(res => res.json().then(
          data=> {
            this.loaded = true;

            if (data.success) {
              this.products = JSON.parse(data.result)
              this.ratings = data.ratings
              console.log(this.products)
            }
            else {
              this.router.navigate([''])
            }
          }
        ))
    })
  }

  inputf() {
    switch (this.filter) 
    {
      case "lowtohigh":
        this.products = this.products.sort((a,b)=> {
          return a.fields.price - b.fields.price
        })
        break;
      case "hightolow":
        this.products = this.products.sort((a,b)=> {
          return b.fields.price - a.fields.price
        })
        break;
      case "toprated":
        this.ratings = this.ratings.sort((a,b)=>{
          return a.rating - b.rating
        })

        for(let i=0; i< this.ratings.length; i++) {
          let obj = this.products.filter(e=>e.pk == this.ratings[i].product_id)
          console.log(obj)
          if (obj != undefined)
          {
            let index = this.products.indexOf(obj[0])
            this.products.splice(index, 1)
            this.products.unshift(obj[0])
          }
        }

        break;
      default:
        break;
    }
  }

  getRating(id) {
    let val = this.ratings.filter(e=>e.product_id == id)[0]
    if (val == undefined) {
      return {rating:0}
    }
    return val
  }

}
