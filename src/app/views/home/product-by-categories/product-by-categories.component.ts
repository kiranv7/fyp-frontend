import { Component, OnInit, Input } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-product-by-categories',
  templateUrl: './product-by-categories.component.html',
  styleUrls: ['./product-by-categories.component.css']
})
export class ProductByCategoriesComponent implements OnInit {
  @Input() products = [];
  bagsArray;
  lapsArray;

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    navSpeed: 1000,
    autoplay:true,
    dragEndSpeed:1000,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      940: {
        items: 2
      }
    },
    nav: false
  }

  constructor() { }

  ngOnInit() {
    
  }

  bags() {
    if ( this.bagsArray == undefined ) {
      this.bagsArray = this.products.filter(
        product => product.sub_category == "Bags"
      )
    }
    return this.bagsArray
  }

  getPrice(price, disc){
    let realPrice = price - ( disc / 100 ) * price;
    return realPrice
  }

  laptops() {
    if (this.lapsArray == undefined ) {
      this.lapsArray = this.products.filter(
        product=> product.sub_category == "Laptops"
      )
    }
    return this.lapsArray
  }


}
