import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart/cart.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  loaded = false;
  cartItems;
  errors;
  carts;

  constructor(
      private cart: CartService, 
      private auth: AuthenticationService, 
      private router: Router,
      private snackbar: MatSnackBar
    ) { }

  ngOnInit() {
    this.fetchCart();
  }


  fetchCart(){
    this.cart.getCartItems().subscribe((data:any)=> {
      this.errors = '';
      this.loaded  = true;
      if(data.success){
        this.cartItems = JSON.parse(data.products)
        this.carts = JSON.parse(data.cart)
      }
      else{
        this.errors = data.message
      }

    })
  }

  remove(slug){
    this.cart.removeCart(slug)
      .subscribe((res:any)=> {
        this.snackbar.open(res.message, "", {duration:3000})
        if(res.success){
          let count = parseInt(window.localStorage.getItem("spioCart"));
          window.localStorage.setItem("spioCart", JSON.stringify(count - 1));
          this.fetchCart();
        }
      })
  }

  viewProduct(slug){
    this.router.navigate([`/product/${slug}`])
  }

}
