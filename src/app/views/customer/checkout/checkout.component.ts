import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatSnackBar, MatDialogRef, MatDialog } from '@angular/material';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { BuyproductService } from 'src/app/services/buyproduct.service';
import { ApiService } from 'src/app/services/api.service';
import { AddNewAddressComponent } from '../account/add-new-address/add-new-address.component';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  response;
  product;
  addresses;
  loaded = false;
  selAddress:any = false;
  bill_price;
  final_price;
  stock;
  selQty:any = 1;
  payment = 'Online';

  constructor(private dialog: MatDialog, private api: ApiService, private dref: MatDialogRef<CheckoutComponent>, private snack: MatSnackBar, @Inject(MAT_DIALOG_DATA) private data:any, private buy: BuyproductService) { }

  ngOnInit() {
    this.buy.checkoutData(this.data.slug, this.data.seller).subscribe(data=>{
      this.loaded = true;
      if(data.success){
        this.response = data
        this.product = JSON.parse(data.product)[0]
        this.addresses = JSON.parse(data.addresses)
        console.log(this.product);
        this.bill_price = data.final_price
        this.final_price = data.final_price
        this.stock = data.stock
      }else{
        this.snack.open(data.message, "OK", {duration:3000})
        this.dref.close()
      }
    })
  }

  quantity(e){
    this.bill_price = this.final_price * e.value;
    this.selQty = e.value  
  }

  pay(){
    let data={
      product_id: this.product.pk,
      quantity: this.selQty,
      amount: this.bill_price,
      address: this.selAddress,
      seller: this.data.seller,
      payment: this.payment
    }
    let form = document.querySelector<any>('#paying_form')
    document.querySelector<any>('#username').value = window.localStorage.getItem('spiouid')
    document.querySelector<any>('#token').value = window.localStorage.getItem('spiotoken')
    document.querySelector<any>('#req_data').value = JSON.stringify(data)
    form.submit()
  }

  addAddress(){
    let dialogRef = this.dialog.open(AddNewAddressComponent, {width:'900px', height:'370px'})
    dialogRef.afterClosed().subscribe(data=>{
      if(data != undefined){
        this.addresses = data
      }
    })
  }

}
