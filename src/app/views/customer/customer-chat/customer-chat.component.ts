import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-customer-chat',
  templateUrl: './customer-chat.component.html',
  styleUrls: ['./customer-chat.component.css']
})
export class CustomerChatComponent implements OnInit {
  loaded=false;
  response;
  ownid;
  closeSocket= false;
  s_auth=true;
  messages;
  chat_indicator;
  actual_response;

  private conn: WebSocket;

  constructor(private auth: AuthenticationService) { }

  ngOnInit() {
    this.chatInit()  
  }


  chatInit(){
    this.response = null
    this.ownid=null

    let token = this.auth.getToken()
    let uid = this.auth.getUserId()

    if(token && uid && this.closeSocket == false){
      let req = {
        req_type:"auth",
        token: token,
        username: uid
      }

      this.conn = new WebSocket("ws://127.0.0.1:8000/customer-chat-socket/")

      this.conn.onopen = (e)=>{
        console.log('opening------------');
        this.conn.send( JSON.stringify(req) )
      }
      
      this.conn.onclose = (event)=>{
        if(this.closeSocket == false){
          this.chatInit()
        }
      }

      this.conn.onmessage = (event)=>{
        this.loaded = true;
        this.chat_indicator =null; 
        this.response = JSON.parse(event.data)
        console.log(this.response);
        
        if(this.response.success == true){
          this.loaded = true
          this.closeSocket = false;
          if(this.response.no_chat == false){
            if(this.response.type == "new_mesg"){
              if(this.actual_response.active_thread.other == this.response.active_thread.other){
                this.actual_response = this.response
                this.messages = JSON.parse(this.actual_response.chat)
                //console.log('mesg from active'); 
                
              }
              else{
                //console.log('mesg from inactive');
                this.chat_indicator = this.response.active_thread.other
              }
            }
            else{
              this.messages = JSON.parse( this.response.chat )
              this.actual_response = this.response
            }
  
            setTimeout(() => {
              let box = document.getElementsByClassName('mesg-sec')[0]
              box.scrollTop = box.scrollHeight
              console.log(box);
              
            }, 10);
          }
          else{
            this.actual_response = this.response
          }
        }
        else{
          this.closeSocket = true;
        }
      }
    }

  }



  sellerSelect(id){
    let req = {
      req_type:"select",
      selected_id:id,
      uid:this.auth.getUserId()
    }
    document.querySelector<any>('.chat-box').value=''

    this.conn.send(JSON.stringify(req))
  }

  sendMsg(){
    let msg = document.querySelector<any>('.chat-box').value
    let req = {
      req_type:"send_mesg",
      message:msg,
      to:this.response.active_thread.other,
      uid:this.auth.getUserId()
    }
    if(msg != ''){
      this.conn.send(JSON.stringify(req))
      document.querySelector<any>('.chat-box').value = ''
    }
  }


}
