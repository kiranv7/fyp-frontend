import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { AddNewAddressComponent } from './add-new-address/add-new-address.component';
import { EditAddressComponent } from './edit-address/edit-address.component';
import { CustomerChatComponent } from '../customer-chat/customer-chat.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  errors:any = null;
  loaded = false;
  cinfo:any;
  addresses; 
  profile:FormGroup;
  showspin= false;
  selected = 'pinfo'


  constructor(private router: Router, private dialog: MatDialog, private api: ApiService, private fb: FormBuilder, private snackbar: MatSnackBar, private auth: AuthenticationService) { }

  ngOnInit() {

    this.api.getCustomerData().subscribe(res=>{
      this.loaded = true
      this.errors = false

      if(res.success){
        this.cinfo = res.profile
        this.addresses = JSON.parse(res.delivery_addresses)
        console.log(res);
        console.log(this.addresses);

        this.profile = this.fb.group({
          email: [this.cinfo.email, Validators.email],
          firstName: [this.cinfo.first_name, Validators.required],
          lastName: [this.cinfo.last_name, Validators.required],
          phone: [this.cinfo.phone, Validators.minLength(10)],
          gender: [this.cinfo.gender, Validators.required]
        })
        
      }
      else{
        this.errors = res.message
      }
    })

  }

  update(){
    this.showspin = true;
    this.api.updateProfile(this.profile.value).subscribe(data=>{
      this.showspin = false;
      this.snackbar.open(data.message, "OK", {duration:3000})
      if(data.success){
        window.localStorage.setItem("spioUsername", this.profile.value.firstName)
        window.localStorage.setItem('spiouid', this.profile.value.email)
      }
    })
  }

  selectMenu(e,opt){
    let active = document.getElementsByClassName('active')
    for(let i =0; i< active.length; i++){
      active[i].classList.remove('active')
    }

    e.target.classList.add('active')
    switch (opt) {
      case 'pinfo':
        this.selected = 'pinfo'
        break;
      
      case 'daddress':
        this.selected = 'daddress'
        break;
      default:
        break;
    }
  }

  addAddress(){
    let dialogRef = this.dialog.open(AddNewAddressComponent, {width:'900px', height:'370px'})
    dialogRef.afterClosed().subscribe(data=>{
      if(data != undefined){
        this.addresses = data
      }
    })
  }

  delete(id){
    this.api.deleteAddress(id).subscribe(data=>{
      this.snackbar.open(data.message, "OK", {duration:3000})
      if(data.success){
        this.addresses = JSON.parse(data.addresses)
      }
    })
  }

  edit(index){
    let ref = this.dialog.open(EditAddressComponent, {width:'900px', height:'370px', data:this.addresses[index]})
    ref.afterClosed().subscribe(data=>{
      if(data != undefined){
        this.addresses = data
      }
    })
  }

  chat(){
    this.dialog.open(CustomerChatComponent, {width:'950px', height:'650px'})
  }

  logout(){
    this.auth.logout().subscribe((data: any)=>{
      if(data.success){
        this.auth.clearLocal();
        this.router.navigate([''])
      }
    })
  }

}
