import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-edit-address',
  templateUrl: './edit-address.component.html',
  styleUrls: ['./edit-address.component.css']
})
export class EditAddressComponent implements OnInit {
  addressForm: FormGroup
  address_id;
  addresses;

  constructor(private snackbar: MatSnackBar, private api: ApiService, @Inject(MAT_DIALOG_DATA) private data:any, private fb: FormBuilder, private dialogref: MatDialogRef<EditAddressComponent>) { }

  ngOnInit() {
    console.log(this.data);
    this.address_id = this.data.pk

    this.addressForm = this.fb.group({
      house_or_office_name: [this.data.fields.house_or_office_name, Validators.required],
      state: [this.data.fields.state, Validators.required],
      landmark: [this.data.fields.landmark, Validators.required],
      pincode: [this.data.fields.pincode, Validators.required],
      district: [this.data.fields.district, Validators.required]
    })
  }

  close(){
    this.dialogref.close()
  }

  editAddress(){
    this.api.editAddress(this.addressForm.value, this.address_id).subscribe(
      data=>{
        this.snackbar.open(data.message, "OK", {duration:3000})
        if(data.message){
          this.addresses = JSON.parse(data.addresses)
          this.dialogref.close(this.addresses)
        }
      }
    )
  }

}
