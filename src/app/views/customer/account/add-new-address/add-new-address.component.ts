import { Component, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { ApiService } from 'src/app/services/api.service';
import { EventEmitter } from 'protractor';
import { JsonPipe } from '@angular/common';
import { of } from 'rxjs';

@Component({
  selector: 'app-add-new-address',
  templateUrl: './add-new-address.component.html',
  styleUrls: ['./add-new-address.component.css']
})
export class AddNewAddressComponent implements OnInit {
  addressForm: FormGroup;
  addresses;

  constructor(private snackbar: MatSnackBar, private api:ApiService, private fb: FormBuilder, private dialogref: MatDialogRef<AddNewAddressComponent>) { }

  ngOnInit() {
    this.addressForm = this.fb.group({
      house_or_office_name: ['', Validators.required],
      state: ['', Validators.required],
      landmark: ['', Validators.required],
      pincode: ['', Validators.required],
      district: ['', Validators.required]
    })
  }

  addAddress(){
    if(this.addressForm.valid){
      this.api.addAddress(this.addressForm.value).subscribe(data=>{
        if(data.success){
          this.snackbar.open(data.message, "OK", {duration:3000})
          this.addresses = JSON.parse(data.addresses)
          this.dialogref.close(this.addresses)
        }
        else{
          this.snackbar.open(data.message, "OK", {duration:3000})
        }
      })
    }
  }

  emitAddress(){
    return of(this.addresses)
  }

  close(){
    this.dialogref.close()
  }

}
