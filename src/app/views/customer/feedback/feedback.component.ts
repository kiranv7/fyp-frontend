import { Component, OnInit, Inject } from '@angular/core';
import { MatSnackBar, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
  selected = 0;
  showspin = false;

  rating = [
    {
      index:1,
      val:'Very Poor'
    },
    {
      index:2,
      val:'Not bad'
    },
    {
      index:3,
      val:'Good'
    },
    {
      index:4,
      val:'Very good'
    },
    {
      index:5,
      val:"Excellent"
    }
  ]

  selIndex;
  orderId;
  
  constructor(private dialogRef: MatDialogRef<FeedbackComponent>, @Inject(MAT_DIALOG_DATA) private data:any, private snack: MatSnackBar, private api: ApiService) { }

  ngOnInit() {
    this.orderId = this.data
    
  }


  starSel(){
    setTimeout(() => {  
      let stars = document.getElementsByClassName('star-templ')

      for(let i =0; i< stars.length; i++){
        stars[i].classList.remove('active-star')
      }

      for(let i =0; i< this.selected; i++){
        stars[i].classList.add('active-star')
        this.selIndex = this.selected;
    }
    }, 100);

  }

  submit(){
    if(this.selected < 1){
      this.snack.open("Rating is required!!!", '', {duration:3000})
    }
    else{
      this.showspin = true
      let comment =  document.querySelector<any>('.comment-box').value
      this.api.sentFeedback(this.orderId, this.selected, comment).subscribe(data=>{
        this.snack.open(data.message, '', {duration:3000})
        this.showspin = false
        if(data.success){
          this.dialogRef.close()
        }       
      })
    }
  }

}
