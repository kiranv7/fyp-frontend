import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { MatDialog } from '@angular/material';
import { OrderViewComponent } from './order-view/order-view.component';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  orders = [];
  products = [];
  orders_exist = false;
  constructor(private auth: AuthenticationService, private dialog: MatDialog) { }

  ngOnInit() {
    this.auth.orders().subscribe((data:any)=>{
     if(data.success){
        console.log(data.orders.length);
        if(data.orders.length > 0){
          this.orders_exist = true
          
          data.orders.forEach(element => {
            this.orders.push( JSON.parse(element) )
          });

          data.products.forEach(element => {
            this.products.push( JSON.parse(element) )
          });

          console.log(this.orders, this.products);
          
        }
        else{
          this.orders_exist = false
        }
        
     }
     else{
       //console.log(data);
       
     }
      
    })
  }


  view(id){
    this.dialog.open(OrderViewComponent, {width:'1000px',data:{order:id}})
  }

}
