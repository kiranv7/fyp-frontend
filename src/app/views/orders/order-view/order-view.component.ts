import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar, MatDialog } from '@angular/material';
import { ApiService } from 'src/app/services/api.service';
import { FeedbackComponent } from '../../customer/feedback/feedback.component';

@Component({
  selector: 'app-order-view',
  templateUrl: './order-view.component.html',
  styleUrls: ['./order-view.component.css']
})
export class OrderViewComponent implements OnInit {
  order;
  loaded = false;
  product;
  address;
  seller_name;
  show_cancel = true;


  constructor(private dialog: MatDialog, private snack: MatSnackBar, private dialodref: MatDialogRef<OrderViewComponent>, @Inject(MAT_DIALOG_DATA) private data:any, private api: ApiService) { }

  ngOnInit() {
    this.api.getOrder(this.data.order).subscribe(
      (res: any)=>{
        this.loaded = true

        if(res.success == true){
          this.order = JSON.parse(res.order)[0]
          this.product = JSON.parse(res.product)[0]
          this.address = JSON.parse(res.address)[0]
          this.seller_name = res.seller_name

          console.log(this.product);
          if(this.order.fields.refund_status == 'Initiated' || this.order.fields.order_status == 'Cancelled'){
            this.show_cancel = false
          }

          if(this.order.fields.refund_status == 'Initiated'){
            this.api.refundStatus(this.order.fields.order_id).subscribe(res=>{
              if(res.success){
                this.order = JSON.parse(res.order)[0]
              }
            })
          }
        }
        
      }
    )
  }

  close(){
    this.dialodref.close()
  }

  cancel(){
    console.log('cancelbtn');
    
    this.api.cancelOrder(this.order.fields.order_id).subscribe((data:any)=>{
      this.snack.open(data.message, "OK", {duration:3000})
      if(data.success == true){
        this.order = JSON.parse(data.order)[0]
      }
    })
  }

  feedback(){
   this.dialog.open(FeedbackComponent, {width:'900px', height:'600px', data:this.order.fields.order_id})
  }

}
