import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, Form } from '@angular/forms';
import { SellerService } from 'src/app/services/seller/seller.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpEventType } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  addProductForm: FormGroup
  selectedFile: File
  categories;
  progress = 0;
  showProgress = false;
  completed = false; 

  constructor(private router: Router, private fb: FormBuilder, private seller: SellerService, public dialogRef: MatDialogRef<AddProductComponent>, @Inject(MAT_DIALOG_DATA) public data:any) { }

  ngOnInit() {
    this.addProductForm = this.fb.group({
      title:['', Validators.required],
      description:['', [Validators.minLength(50), Validators.required]],
      category:['', [Validators.required]],
      sub_category:['', Validators.required],
      price:['', Validators.required],
      discount_percentage: ['', [Validators.required, Validators.maxLength(3)]],
      stock: ['', Validators.required]
    })

    this.categories = Object.entries(this.data);
    
  }

  addProduct(){
    if(this.selectedFile == undefined){
      alert("Add a product image")
    }
    else{
      
      this.seller.addProduct(this.addProductForm.value, this.selectedFile).subscribe((data:any)=>{
        this.addProductForm.reset()
        this.showProgress = true

        if(data.type === HttpEventType.UploadProgress){
          this.progress = Math.round(data.loaded / data.total * 100);
        }
        else if(data.type === HttpEventType.Response){
          
          this.showProgress = false
          if(data.body.success == true){
            this.completed = true
            this.router.navigate(['seller'])
          }
        }
      })
    }
  }

  addFile(e){
    this.selectedFile = e.target.files[0]
  }

  close(){
    this.dialogRef.close()
  }
}
