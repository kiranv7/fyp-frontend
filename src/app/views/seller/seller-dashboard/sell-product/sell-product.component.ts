import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { MatDialog } from '@angular/material';
import { AlternateSellerComponent } from './alternate-seller/alternate-seller.component';

@Component({
  selector: 'app-sell-product',
  templateUrl: './sell-product.component.html',
  styleUrls: ['./sell-product.component.css']
})
export class SellProductComponent implements OnInit {
  slug;
  product;
  loaded= false;
  prod_price;

  constructor(private route: ActivatedRoute, private api: ApiService, private router: Router, private dialog: MatDialog) { }

  ngOnInit() {
    this.route.paramMap.subscribe((param:any)=>{
      this.slug = param.params.slug

      this.api.getProduct(this.slug).subscribe((data:any)=> {
        this.product = data
        this.loaded = true;
      })
    })

  }

  bindEvent() {
   setTimeout( ()=> {
      document.getElementsByClassName('popover-body')[0].addEventListener("click", ()=> {
      let id = document.getElementsByClassName('popover-body')[0].parentElement.previousElementSibling.id
      console.log(id);
      this.router.navigate(['/view-seller/',id])
    })
   }, 150)
  }

  getRealPrice(price, disc){
    if(disc > 0){
      return this.prod_price = parseInt(price) - parseInt(price) * (parseInt(disc) / 100)
    }
    else{
      return this.prod_price = price;
    }
  }

  altSeller() {
    this.dialog.open(AlternateSellerComponent, {data:{
      product: this.product
    }, width:'650px', height:'220px'})
  }


}
