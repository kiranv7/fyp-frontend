import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlternateSellerComponent } from './alternate-seller.component';

describe('AlternateSellerComponent', () => {
  let component: AlternateSellerComponent;
  let fixture: ComponentFixture<AlternateSellerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlternateSellerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlternateSellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
