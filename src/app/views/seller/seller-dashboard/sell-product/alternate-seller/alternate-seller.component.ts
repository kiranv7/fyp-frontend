import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { SellerService } from 'src/app/services/seller/seller.service';

@Component({
  selector: 'app-alternate-seller',
  templateUrl: './alternate-seller.component.html',
  styleUrls: ['./alternate-seller.component.css']
})
export class AlternateSellerComponent implements OnInit {
  success = false;

  constructor(private seller: SellerService, private dialogRef: MatDialogRef<AlternateSellerComponent>, @Inject(MAT_DIALOG_DATA) private data, private snack: MatSnackBar) { }

  ngOnInit() {
    
  }

  close() {
    this.dialogRef.close()
  }

  submit() {
    let price:any = document.getElementById('sellingprice')
    let stock:any = document.getElementById('stock')

    if ( price.value > this.data.product.price ) {
      this.snack.open(`Your selling price shouldn't be greater than ${this.data.product.price} !`, '', {duration:3000})
    }
    else if (stock.value == undefined) {
      this.snack.open('Enter the available stock', '', {duration:3000})
    }
    else {
      this.seller.altSellerReq(stock.value, price.value, this.data.product.slug)
        .subscribe((data:any)=>{
          this.snack.open(data.message, '', {duration:3000})
          if(data.success) {
            this.success = true
          }
        })
    }
  }


}
