import { Component, OnInit } from '@angular/core';
import { SellerService } from 'src/app/services/seller/seller.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { AddProductComponent } from './add-product/add-product.component';
import { DeleteProductComponent } from './delete-product/delete-product.component';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { async } from '@angular/core/testing';
import { ApiService } from 'src/app/services/api.service';
import { EditProductComponent } from './edit-product/edit-product.component';
import { PendingOrdersComponent } from '../pending-orders/pending-orders.component';
import { EditAltProductComponent } from './edit-alt-product/edit-alt-product.component';

@Component({
  selector: 'app-seller-dashboard',
  templateUrl: './seller-dashboard.component.html',
  styleUrls: ['./seller-dashboard.component.css']
})
export class SellerDashboardComponent implements OnInit {
  loaded = false;
  response;
  products;
  orders_count;
  alt_products;

  constructor(private api: ApiService, private seller: SellerService, private router: Router, private dialog: MatDialog, private auth: AuthenticationService) { }

  ngOnInit() {
    this.getData()

    setInterval(this.getData, 30000)
  }

  getData = ()=>{
    this.seller.getSellerData().subscribe(data=>{
      this.loaded= true;
      if(data.success == true){
        this.response = data
        this.orders_count = data.order_count
        if(data.verified == false){
          this.router.navigate(['seller/verify'])
        }
        else{
          switch (data.status) {
            case 'pending':
              this.router.navigate(['seller/under-review']);
              break;
            case 'rejected':
              this.router.navigate(['seller/rejected'])
            default:
              this.products = JSON.parse(data.products)
              break;
          }
        }
      }
      else{
        this.router.navigate(['seller'])
      }
    })
  }

  

  addProduct(){
    this.dialog.open(AddProductComponent, {width:'650px', height:'600px', data:JSON.parse(this.response.categories)})
  }

  delete(slug, title, type='seller'){
    this.dialog.open(DeleteProductComponent, {width:'650px', data:{slug:slug, title:title, type:type}})
  }

  edit(slug){
    this.api.getProduct(slug).subscribe(data=>{
      this.dialog.open(EditProductComponent, {width:'900px', height:'600px', data:data})
    })
  }

  pendingOrders(){
    this.dialog.open(PendingOrdersComponent, {width:'900px', height:'600px'})
  }

  successfulOrders(id){
    let o = this.orders_count.filter(e=>e.product_id == id)
    if ( o[0] == undefined ) {
      return {orders: 0}
    }
    else
      return o[0] 
  }
  
  getParsed(json) {
    return JSON.parse(json)[0]
  }

  editAlt(id,price) {
    this.dialog.open(EditAltProductComponent, {width:'700px', height:'500px', data:{id:id,price:price}})
  }

}
