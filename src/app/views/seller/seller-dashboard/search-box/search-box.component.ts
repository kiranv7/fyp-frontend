import { Component, OnInit } from '@angular/core';

import { fromEvent } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { debounceTime, distinctUntilChanged, filter, map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css']
})
export class SearchBoxComponent implements OnInit {
  response = null;

  constructor() { }

  ngOnInit() {

    const searchBox:any = document.getElementById('search-box');

    searchBox.addEventListener("focusout", ()=>{
      setTimeout( ()=>{
        console.log('focusout')
        this.response = null;
      }, 2000)
    })

    const typeahead = fromEvent(searchBox, 'input').pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 2),
      debounceTime(10),
      distinctUntilChanged(),
      switchMap(() => { 
        this.response = null;
        return ajax(`http://127.0.0.1:8000/api/search-product/?input=${searchBox.value}`)
      })
    );

    typeahead.subscribe(data => {
      // Handle the data from the API
      if(data.response.success) {
        this.response = JSON.parse(data.response.result)
        console.log(this.response)
      }
    });

  }

}
