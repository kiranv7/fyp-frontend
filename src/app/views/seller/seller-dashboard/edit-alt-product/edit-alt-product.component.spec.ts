import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAltProductComponent } from './edit-alt-product.component';

describe('EditAltProductComponent', () => {
  let component: EditAltProductComponent;
  let fixture: ComponentFixture<EditAltProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAltProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAltProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
