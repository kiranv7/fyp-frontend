import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { SellerService } from 'src/app/services/seller/seller.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-edit-alt-product',
  templateUrl: './edit-alt-product.component.html',
  styleUrls: ['./edit-alt-product.component.css']
})
export class EditAltProductComponent implements OnInit {
  loaded= false;
  selling_info: FormGroup;
  success= false;

  constructor(private snack: MatSnackBar, private dialogRef: MatDialogRef<EditAltProductComponent>, @Inject(MAT_DIALOG_DATA) private data, private seller: SellerService, private fb: FormBuilder) { }

  ngOnInit() {
    this.selling_info = this.fb.group({
      stock:[null, Validators.required],
      price:[null, Validators.required]
    })

    this.seller.getAltInfo(this.data.id)
      .subscribe(data=> {
        this.loaded = true;

        if(data.success) {
          let price = this.data.price
          let disc = JSON.parse(data.info)[0].fields.discount_percentage

          price = price - (( disc / 100 ) * price )

          this.selling_info.setValue(
            {
              stock:JSON.parse(data.info)[0].fields.stock, 
              price:price
            }
          )
        }
      })
  }


  submit(e) {
    e.preventDefault()
    this.seller.setAlt(this.data.id, this.selling_info.value.price, this.selling_info.value.stock)
      .subscribe(data=> {
        console.log(data)
        if(data.success) {
          this.success = true;
          this.snack.open(data.message, '', {duration:3000})
        }
        else{
          this.snack.open(data.message, '', {duration:3000})
        }
      })
  }

  cancel() {
    this.dialogRef.close()
  }



}
