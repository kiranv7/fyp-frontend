import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SellerService } from 'src/app/services/seller/seller.service';
import { HttpEventType } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})

export class EditProductComponent implements OnInit {
  private showForm = false;
  editForm: FormGroup
  selectedFile:any = null; 
  showProgress = false;
  progress:any;
  completed = false;

  constructor(private dialogRef: MatDialogRef<EditProductComponent>, private router: Router, private seller: SellerService, @Inject(MAT_DIALOG_DATA) private data:any, private fb: FormBuilder) { }

  ngOnInit() {
    let sellerid = window.localStorage.getItem('sellerId') ?  window.localStorage.getItem('sellerId') : false

    if( sellerid == this.data.seller.user.username ) {
      this.editForm = this.fb.group({
        title:[this.data.title, Validators.required],
        description:[this.data.description, [Validators.minLength(50), Validators.required]],
        category:[this.data.category, [Validators.required]],
        sub_category:[this.data.sub_category, Validators.required],
        price:[this.data.price, Validators.required],
        discount_percentage: [this.data.discount_percentage, [Validators.required, Validators.maxLength(3)]],
        stock: [this.data.stock, Validators.required]
      })
    }
    else{
      
    }
  }

 

  addFile(e){
    this.selectedFile = e.target.files[0]
  }

  editProduct(){
      this.seller.editProduct(this.editForm.value, this.selectedFile, this.data.id).subscribe((data:any)=>{
        this.editForm.reset()
        this.showProgress = true

        if(data.type === HttpEventType.UploadProgress){
          this.progress = Math.round(data.loaded / data.total * 100);
        }
        else if(data.type === HttpEventType.Response){
          
          this.showProgress = false
          if(data.body.success == true){
            this.completed = true
            this.router.navigate(['seller'])
          }
        }
      })
  }


  close(){
    this.dialogRef.close()
  }

}
