import { Component, OnInit } from '@angular/core';
import { SellerService } from 'src/app/services/seller/seller.service';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-seller-chat',
  templateUrl: './seller-chat.component.html',
  styleUrls: ['./seller-chat.component.css']
})
export class SellerChatComponent implements OnInit {
  loaded = false;
  msgs= [];
  threads;
  chat_error;
  res;
  selectedChat;
  curChatUser;
  ownId;
  closeSocket = false;
  private conn: WebSocket;

  constructor(private seller: SellerService) { }

  ngOnInit() {
    this.getSellerMsg();
    //var chat = new WebSocket("ws://")
  }

  getSellerMsg(){
    this.conn = new WebSocket("ws://127.0.0.1:8000/seller-chat-socket/")
    
    let req = {
      req_type:"auth",
      token: this.seller.getSellerToken,
      username: this.seller.getSellerUsername
    }


    this.conn.onopen = (event)=>{
      this.conn.send( JSON.stringify(req) )
      console.log(event);
      
    }

    this.conn.onclose = (event)=>{
      if(this.closeSocket == false){
        this.getSellerMsg()
      }

    }

    this.conn.onmessage = (event)=>{
      this.res = JSON.parse(event.data)
      this.msgs = JSON.parse(this.res.chat)
      console.log(this.msgs);
      this.ownId = this.res.id
      this.loaded = true;
      this.curChatUser = this.res.customers[0].id

      this.msgs.map(data=>{
        
        if(this.res.customers[0].id == data.fields.user){
          this.selectedChat = data.fields.thread
        }
      })

      this.msgs = this.msgs.reverse()
      setTimeout(() => {
        let box = document.getElementsByClassName('mesg-sec')[0]
        box.scrollTop = box.scrollHeight
        console.log(box);
        
      }, 10);
    }
  }

  sendMsg(){
    let messg = document.querySelector<any>('.chat-box').value;
    console.log(messg);
    let req={
      req_type:"send_mesg",
      message:messg,
      to:this.curChatUser
    }
    if(messg != ''){
      this.conn.send(JSON.stringify(req))
      document.querySelector<any>('.chat-box').value= ''
    }
  }

  custSelect(val){
    this.curChatUser = val;

    this.msgs.map(data=>{
      if(data.fields.user == val){
        this.selectedChat = data.fields.thread
      }
    })
    
  }


}
