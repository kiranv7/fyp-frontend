import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { SellerService } from 'src/app/services/seller/seller.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delete-product',
  templateUrl: './delete-product.component.html',
  styleUrls: ['./delete-product.component.css']
})
export class DeleteProductComponent implements OnInit {
  title;
  slug;
  showProgress = false;
  success;
  error;
  message;
  type;

  constructor(private router: Router, @Inject(MAT_DIALOG_DATA) private data:any, private seller: SellerService, private dialogRef: MatDialogRef<DeleteProductComponent>) { }

  ngOnInit() {
    this.title = this.data.title;
    this.slug = this.data.slug;
    this.type = this.data.type
  }

  deletep(){
    this.showProgress = true
    this.error = ''
    this.message = ''

    this.seller.deleteProduct(this.slug, this.type).subscribe(
      (data:any)=>{
        this.showProgress = false
        if(data.success){
          this.success = true
          this.message = data.message

          this.router.navigate(['seller'])
        }
        else{
          this.success = false
          this.error = data.message
        }
      }
    )
  }

  cancel(){
    this.dialogRef.close()
  }

}
