import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { SellerService } from 'src/app/services/seller/seller.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-revenue',
  templateUrl: './revenue.component.html',
  styleUrls: ['./revenue.component.css']
})
export class RevenueComponent implements OnInit {
  loaded = false;
  earning;


  constructor(private sellerApi: SellerService, private snack: MatSnackBar) { }

  ngOnInit() {
    this.sellerApi.getEarnings()
      .subscribe(data=>{
        this.loaded = true;
        if(data.success) {
          this.earning = data.earnings
        }
        else{
          this.snack.open(data.message)
        }
      })
  }

}
