import { Component, OnInit } from '@angular/core';
import { SellerService } from 'src/app/services/seller/seller.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { SetDeliveryDateComponent } from './set-delivery-date/set-delivery-date.component';

@Component({
  selector: 'app-pending-orders',
  templateUrl: './pending-orders.component.html',
  styleUrls: ['./pending-orders.component.css']
})
export class PendingOrdersComponent implements OnInit {
  orders: any = false;
  loaded = false;
  products;
  addresses;
  no_pending_orders = false;

  constructor(private dialog: MatDialog, private snack: MatSnackBar, private seller: SellerService) { }

  ngOnInit() {
    this.seller.pendingOrders()
      .subscribe(data=>{
        this.loaded = true;
        
        if ( data.success && data.pending_orders ) {
          this.orders = JSON.parse(data.orders)
          this.products = JSON.parse(data.products)
          this.addresses = JSON.parse(data.addresses)
        }
        else if(data.pending_orders == false) {
          this.no_pending_orders = true
        }
        else {
          this.snack.open(data.message)
        }
        console.log(this.orders);
        
      })
  }


  product(id) {
    let p = this.products.filter(e=> e['pk'] == id)
    return p[0];
  }

  deliveryDate(id){
    let order = this.orders.filter( e=> e['pk'] == id )[0]
    
    let address = this.addresses.filter( e=> e['pk'] == order.fields.delivery_address )[0]

    let data = {
      curr_date: order.fields.expected_delivery,
      address: address,
      order_id: order.fields.order_id
    }

    this.dialog.open(SetDeliveryDateComponent, {width:'800px',data:data})
  }


}
