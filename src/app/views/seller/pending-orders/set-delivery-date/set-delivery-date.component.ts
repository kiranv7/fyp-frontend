import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { SellerService } from 'src/app/services/seller/seller.service';

@Component({
  selector: 'app-set-delivery-date',
  templateUrl: './set-delivery-date.component.html',
  styleUrls: ['./set-delivery-date.component.css']
})
export class SetDeliveryDateComponent implements OnInit {
  selDate = {
    year: null,
    day: null,
    month: null
  };

  status;

  order_statuses = [
    'Cancelled',
    'Shipped',
    'Delivered'
  ]

  constructor(@Inject(MAT_DIALOG_DATA) private data: any, private snack: MatSnackBar, private seller: SellerService) { }

  ngOnInit() {
   
  }

  addDate(e){
    this.selDate.day = e.value.getDate()
    this.selDate.month = e.value.getMonth() + 1;
    this.selDate.year = e.value.getFullYear()
  }

  submit(){
    if( this.selDate.day == null || this.selDate.month == null || this.selDate.year == null || this.status == null){
      this.snack.open("please select date and status!", '', {duration:3000})
    }
    else{
      this.seller.setExpectedDelivery(this.selDate.day, this.selDate.month, this.selDate.year, this.status, this.data.order_id)
        .subscribe(res=>{
          this.snack.open(res.message, '', {duration:3000})
        })
    }

  }

}
