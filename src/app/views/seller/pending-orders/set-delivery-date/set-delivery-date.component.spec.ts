import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetDeliveryDateComponent } from './set-delivery-date.component';

describe('SetDeliveryDateComponent', () => {
  let component: SetDeliveryDateComponent;
  let fixture: ComponentFixture<SetDeliveryDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetDeliveryDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetDeliveryDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
