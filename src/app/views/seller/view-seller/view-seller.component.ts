import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SellerService } from 'src/app/services/seller/seller.service';
import { MatDialog } from '@angular/material';
import { StartchatComponent } from 'src/app/dialogs/startchat/startchat.component';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { LoginAndSignupComponent } from 'src/app/dialogs/login-and-signup/login-and-signup.component';

@Component({
  selector: 'app-view-seller',
  templateUrl: './view-seller.component.html',
  styleUrls: ['./view-seller.component.css']
})
export class ViewSellerComponent implements OnInit {
  sellerId;
  res;
  loaded= false;

  constructor(private auth:AuthenticationService, private dialog: MatDialog,private route: ActivatedRoute, private seller: SellerService, private router: Router) { }

  ngOnInit() {
    this.sellerId = this.route.params.subscribe(params=>{
      this.sellerId = params.id
      this.seller.getSellerInfo(this.sellerId).subscribe(
        (data:any)=>{
          this.loaded = true
          if(data.success == true)
            this.res = data
          else
            this.router.navigate([''])
        }
      )
    })
  }

  chatToSeller(id, name){
    if (this.auth.getisLoggedin() == false){
      this.dialog.open(LoginAndSignupComponent, {width:'650px'})
    }
    else
      this.dialog.open(StartchatComponent, {width:'650px',data:{id:id, name:name}})
  }

}
