import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { SellerLoginComponent } from 'src/app/dialogs/seller-login/seller-login.component';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { SellerChatComponent } from '../seller-dashboard/seller-chat/seller-chat.component';
import { SellerService } from 'src/app/services/seller/seller.service';

@Component({
  selector: 'app-seller-nav',
  templateUrl: './seller-nav.component.html',
  styleUrls: ['./seller-nav.component.css']
})
export class SellerNavComponent implements OnInit {
  socket = {closed:false, connected:false, res:''};

  constructor(private snack: MatSnackBar, private dialog: MatDialog, private auth: AuthenticationService, private router: Router, private seller: SellerService) { }
  sellerLoggedIn;
  chatLength = 0;

  ngOnInit() {
    this.sellerLoggedIn = this.getLoggedIn()
    if(this.sellerLoggedIn == true){
      this.getChatNotif()
    }
  }

  ngOnChanges(){
    this.sellerLoggedIn = this.getLoggedIn()
  }

  getChatNotif(){
    let conn = new WebSocket("ws://127.0.0.1:8000/seller-chat/")
    this.socket.connected = true

    let req = {
      req_type:"auth",
      token: this.seller.getSellerToken,
      username: this.seller.getSellerUsername
    }

    conn.onopen = (event)=>{
      conn.send( JSON.stringify(req) )
      console.log(event);
      
    }

    conn.onclose = (event)=>{
      this.socket.closed = true;
      console.log('close', event);
      this.getChatNotif()
      
    }

    conn.onmessage = (event)=>{
      let res = JSON.parse(event.data)
      this.chatLength = res.chats
      console.log(event, res);
      if(res.type == "notification"){
        this.snack.open(`You have one new message from ${res.customer}.`, "OK", {
          duration: 3000,
        })
      }
    }

  }

  get getChatLenght(){
    if (this.chatLength == 0){
      return undefined
    }
    else{
      return this.chatLength
    }
  }

  getLoggedIn(){
    return window.localStorage.getItem('sellerLoggedIn') ? true : false;
  }

  sellerLoginDialog(){
    this.dialog.open(SellerLoginComponent,{width: '650px'})
  }

  get sellerName(){
    return window.localStorage.getItem('sellerName')
  }

  sellerLogout(){
    this.auth.clearSellerLocal()
    this.router.navigate(['/seller'])
  }

  chat(){
    this.dialog.open(SellerChatComponent, {width:'950px', height:'650px'})
  }

}
