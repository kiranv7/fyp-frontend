import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerLoginBannerComponent } from './seller-login-banner.component';

describe('SellerLoginBannerComponent', () => {
  let component: SellerLoginBannerComponent;
  let fixture: ComponentFixture<SellerLoginBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerLoginBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerLoginBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
