import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { SellerRegisterComponent } from 'src/app/dialogs/seller-register/seller-register.component';

@Component({
  selector: 'app-seller-login-banner',
  templateUrl: './seller-login-banner.component.html',
  styleUrls: ['./seller-login-banner.component.css']
})
export class SellerLoginBannerComponent implements OnInit {

  constructor(private router: Router, private dialog: MatDialog) { 
    let sellerlogin = window.localStorage.getItem('sellerLoggedIn') ? true : false;
    if(sellerlogin){
      router.navigate(['/seller/dashboard'])
    }
  }

  ngOnInit() {
  }

  sellerRegister(){
    this.dialog.open(SellerRegisterComponent, {width:'800px'})
  }

}
