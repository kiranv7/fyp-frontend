import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerVerifyComponent } from './seller-verify.component';

describe('SellerVerifyComponent', () => {
  let component: SellerVerifyComponent;
  let fixture: ComponentFixture<SellerVerifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerVerifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerVerifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
