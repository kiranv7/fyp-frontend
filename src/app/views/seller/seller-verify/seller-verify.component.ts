import { Component, OnInit, ViewChild } from '@angular/core';
import { SellerService } from 'src/app/services/seller/seller.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-seller-verify',
  templateUrl: './seller-verify.component.html',
  styleUrls: ['./seller-verify.component.css']
})
export class SellerVerifyComponent implements OnInit {
  otpSent = false;
  resentOtp = false;
  error;
  constructor(private seller: SellerService, private router: Router) { }

  ngOnInit() {
  }

  onOtpChange(val){
    if(val.length == 4 && !this.otpSent){
      console.log('submit');
      this.otpSent = true
      this.seller.verifyOtp(val).subscribe((data:any)=>{
        this.otpSent = false
        if(data.success == true){
          switch (data.status) {
            case 'pending':
              this.router.navigate(['seller/under-review'])
              break;
            case 'rejected':
              this.router.navigate(['seller/rejected'])
              break;
            case 'approved':
              this.router.navigate(['seller/dashboard'])
              break;
          }
        }
        else{
          this.error = data.message
        }
      })
    }
    else{
      this.otpSent = false
      this.error = ''
    }
  }

  resend(){
    let btn = document.getElementById("resentBtn")
    this.seller.resendOtp().subscribe((data:any)=>{
      if(data.success == false){
        this.error = data.message
      }
    })
    btn.innerHTML = "Check your email"
    this.resentOtp = true
  }

}
