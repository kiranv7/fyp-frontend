import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-rating-view',
  templateUrl: './rating-view.component.html',
  styleUrls: ['./rating-view.component.css']
})
export class RatingViewComponent implements OnInit {
  loaded = false;
  review_data = {
    success:null,
    reviews:null,
    customers:null
  };
  @Input() product;

  constructor(private api: ApiService, private snackbar: MatSnackBar) { }

  ngOnInit() {
    
    this.api.getReviews(this.product).subscribe(
      (data:any) => {
        this.loaded= true
        if (data.success) {
          this.review_data = data
          this.review_data.reviews = JSON.parse(this.review_data.reviews)
        }
        else {
          this.snackbar.open(data.message, '', {duration:3000})
        }
      }
    )

  }

  commentTitle(rating) {
    if( rating <= 1 ) {
      return "Very Poor"
    }
    else if ( rating <= 2 ) {
      return "Not Bad"
    }
    else if( rating <= 3 ) {
      return "Good"
    }
    else if ( rating <= 4 ) {
      return "Very Good"
    }
    else {
      return "Excellent"
    }
  }

  getFname(id) {
    return this.review_data.customers.filter(e=>e.id == id)[0].first_name
  }

}
