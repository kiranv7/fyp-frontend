import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { CartService } from 'src/app/services/cart/cart.service';
import { EEXIST } from 'constants';
import { MatSnackBar, MatDialog } from '@angular/material';
import { CheckoutComponent } from '../customer/checkout/checkout.component';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { LoginAndSignupComponent } from 'src/app/dialogs/login-and-signup/login-and-signup.component';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent implements OnInit {
  slug;
  product;
  alternate_sellers;
  prod_price;
  flag=true;
  selectedSeller;
  

  constructor(private auth: AuthenticationService, private dialog: MatDialog, private route: ActivatedRoute,private router: Router , private api: ApiService, public cart: CartService, private snackbar: MatSnackBar) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.slug = params.slug

      this.api.getProduct(this.slug)
      .subscribe((data:any)=>{
        this.product = data;
        this.selectedSeller = this.product.seller.id
        this.alternate_sellers = data.alternate_sellers
      })
    })

  }

  viewSeller(r){
    
    try {
      document.getElementsByClassName('popover-body')[0].addEventListener('click', function(){
        let id = document.getElementsByClassName('popover-body')[0].parentElement.previousElementSibling.id
        console.log(id);
        r.navigate(['/view-seller/',id])
      })
    } catch (error) {
      
    }
  }


  getRealPrice(price, disc){
    if(disc > 0){
      return this.prod_price = parseInt(price) - parseInt(price) * (parseInt(disc) / 100)
    }
    else{
      return this.prod_price = price;
    }
  }

  addCart(p_name,p_id){
    this.cart.addToCart(p_id).subscribe((data:any)=>{
      if(data.success == true){
        window.localStorage.setItem("spioCart", data.cart_count)

        this.snackbar.open(p_name + " is added to Cart", "Ok", {
          duration: 2000,
        });
      }
      else{
        this.snackbar.open("unable to add!", "Ok", {
          duration: 2000,
        });
      }
    })
  }

  sellerClick(){
    setTimeout(() => {
      this.viewSeller(this.router)
    }, 500);
  }

  changeSeller(index, id,e){
    let tiked = document.getElementsByClassName('tik-badge')

    if(e.classList.contains('tik-badge')){
      e.classList.remove('tik-badge')
      let price = this.getRealPrice(this.product.price, this.product.discount_percentage)
      this.changePrice(price)
      this.selectedSeller= this.product.seller.id
    }
    else{

      if(tiked.length > 0){
        this.removeSellerTik()
      }

      e.classList.add('tik-badge')
      this.selectedSeller = id
      let price = this.getRealPrice(this.prod_price, this.alternate_sellers[index].discount_percentage)
      this.changePrice(price)
    }
    
  }

  changePrice(price){
    document.getElementById('price_tag').innerHTML = '<span>Buy at '+ price +'/-</span>';
  }


  removeSellerTik(){
    let tiked = document.getElementsByClassName('tik-badge')

    for(let i=0;i < tiked.length; i++){
      tiked[i].classList.remove('tik-badge')
    }
  }

  buy(slug){
    if(this.auth.getisLoggedin() && this.selectedSeller != undefined){
      let req = {
        slug: slug,
        seller: this.selectedSeller
      }
      this.dialog.open(CheckoutComponent, {width:'950px', height:'650px', data:req})
    }
    else{
      this.dialog.open(LoginAndSignupComponent, {width:'650px'})
    }
  }

}
