import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatMenuModule, MatButtonModule, MatIconModule, MatRippleModule, MatButtonToggleModule, MatBadgeModule, MatProgressSpinnerModule, MatProgressBarModule, MatToolbarModule, MatSidenavModule, MatListModule, MatDividerModule, MatGridListModule, MatExpansionModule, MatCardModule, MatInputModule, MatTreeModule, MatDialogModule, MatSelectModule, MatSnackBarModule, MatChipsModule, MatPaginatorModule, MatStepperModule, MatDatepicker, MatDatepickerModule, MatNativeDateModule, MatCheckboxModule, MatRadioModule } from '@angular/material';

const materialComponents = [
  MatButtonModule,
  MatButtonToggleModule,
  MatIconModule,
  MatBadgeModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatToolbarModule,
  MatSidenavModule,
  MatMenuModule,
  MatListModule,
  MatDividerModule,
  MatGridListModule,
  MatRippleModule,
  MatExpansionModule,
  MatCardModule,
  MatInputModule,
  MatTreeModule,
  MatRippleModule,
  MatDialogModule,
  MatSelectModule,
  MatProgressBarModule,
  MatSnackBarModule,
  MatChipsModule,
  MatPaginatorModule,
  MatStepperModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCheckboxModule,
  MatRadioModule
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
   [materialComponents]
  ],
  exports :[ materialComponents ]
})
export class MaterialModule { }
