import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './views/home/home.component';
import { OrdersComponent } from './views/orders/orders.component';
import { AuthGuard } from './guards/auth.guard';
import { ProductViewComponent } from './views/product-view/product-view.component';
import { SellerComponent } from './views/seller/seller.component';
import { SellerDashboardComponent } from './views/seller/seller-dashboard/seller-dashboard.component';
import { SellerNavComponent } from './views/seller/seller-nav/seller-nav.component';
import { SellerLoginBannerComponent } from './views/seller/seller-login-banner/seller-login-banner.component';
import { SellerDashboardGuard } from './guards/seller-dashboard.guard';
import { SellerVerifyComponent } from './views/seller/seller-verify/seller-verify.component';
import { AdminApprovalComponent } from './views/seller/admin-approval/admin-approval.component';
import { RejectedComponent } from './views/seller/rejected/rejected.component';
import { ViewSellerComponent } from './views/seller/view-seller/view-seller.component';
import { AccountComponent } from './views/customer/account/account.component';
import { SellProductComponent } from './views/seller/seller-dashboard/sell-product/sell-product.component';
import { SearchResultComponent } from './views/home/search-result/search-result.component';
import { CartComponent } from './views/cart/cart.component';


const routes: Routes = [
  {
    path:'', 
    component: HomeComponent 
  },
  {
    path: 'search/:input',
    component: SearchResultComponent
  },
  { 
   path: 'product/:slug',
   component: ProductViewComponent
  },
  {
    path: 'orders', 
    component: OrdersComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'account',
    component: AccountComponent,
    canActivate: [AuthGuard]
  },
  {
    path:'view-seller/:id',
    component:ViewSellerComponent
  },
  {
    path:'cart',
    component: CartComponent,
    canActivate: [AuthGuard]
  },
  {
    path:'seller',
    component:SellerComponent,
    children:[
      {
        path:'',
        component:SellerLoginBannerComponent,
      },
      {
        path:'dashboard',
        component:SellerDashboardComponent,
        canActivate:[SellerDashboardGuard]
      },
      {
        path:'dashboard/sell-product/:slug',
        component: SellProductComponent,
        canActivate:[SellerDashboardGuard]
      },
      {
        path:'verify',
        component:SellerVerifyComponent,
        canActivate:[SellerDashboardGuard]
      },
      {
        path:'under-review',
        component:AdminApprovalComponent,
        canActivate:[SellerDashboardGuard]
      },
      {
        path:'rejected',
        component:RejectedComponent,
        canActivate:[SellerDashboardGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
