import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CarouselModule } from 'ngx-owl-carousel-o';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './views/home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './modules/material/material.module';
import { NavbarComponent } from './views/navbar/navbar.component';
import { LoginAndSignupComponent } from './dialogs/login-and-signup/login-and-signup.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OrdersComponent } from './views/orders/orders.component';
import { ProductViewComponent } from './views/product-view/product-view.component';
import { SellerComponent } from './views/seller/seller.component';
import { SellerNavComponent } from './views/seller/seller-nav/seller-nav.component';
import { SellerDashboardComponent } from './views/seller/seller-dashboard/seller-dashboard.component';
import { SellerLoginBannerComponent } from './views/seller/seller-login-banner/seller-login-banner.component';
import { SellerLoginComponent } from './dialogs/seller-login/seller-login.component';
import { SellerRegisterComponent } from './dialogs/seller-register/seller-register.component';
import { SellerVerifyComponent } from './views/seller/seller-verify/seller-verify.component';
import { NgOtpInputModule } from  'ng-otp-input';
import { AdminApprovalComponent } from './views/seller/admin-approval/admin-approval.component';
import { RejectedComponent } from './views/seller/rejected/rejected.component';
import { AddProductComponent } from './views/seller/seller-dashboard/add-product/add-product.component';
import { DeleteProductComponent } from './views/seller/seller-dashboard/delete-product/delete-product.component';
import { SellerChatComponent } from './views/seller/seller-dashboard/seller-chat/seller-chat.component';
import { ViewSellerComponent } from './views/seller/view-seller/view-seller.component';
import { StartchatComponent } from './dialogs/startchat/startchat.component';
import { CustomerChatComponent } from './views/customer/customer-chat/customer-chat.component';
import { AccountComponent } from './views/customer/account/account.component';
import { AddNewAddressComponent } from './views/customer/account/add-new-address/add-new-address.component';
import { EditAddressComponent } from './views/customer/account/edit-address/edit-address.component';
import { CheckoutComponent } from './views/customer/checkout/checkout.component';
import { OrderViewComponent } from './views/orders/order-view/order-view.component';
import { FeedbackComponent } from './views/customer/feedback/feedback.component';
import { EditProductComponent } from './views/seller/seller-dashboard/edit-product/edit-product.component';
import { PendingOrdersComponent } from './views/seller/pending-orders/pending-orders.component';
import { SetDeliveryDateComponent } from './views/seller/pending-orders/set-delivery-date/set-delivery-date.component';
import { BannerComponent } from './views/home/banner/banner.component';
import { RatingViewComponent } from './views/product-view/rating-view/rating-view.component';
import { RevenueComponent } from './views/seller/seller-dashboard/revenue/revenue.component';
import { SearchBoxComponent } from './views/seller/seller-dashboard/search-box/search-box.component';
import { SellProductComponent } from './views/seller/seller-dashboard/sell-product/sell-product.component';
import { AlternateSellerComponent } from './views/seller/seller-dashboard/sell-product/alternate-seller/alternate-seller.component';
import { EditAltProductComponent } from './views/seller/seller-dashboard/edit-alt-product/edit-alt-product.component';
import { SearchResultComponent } from './views/home/search-result/search-result.component';
import { ProductByCategoriesComponent } from './views/home/product-by-categories/product-by-categories.component';
import { CartComponent } from './views/cart/cart.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    LoginAndSignupComponent,
    OrdersComponent,
    ProductViewComponent,
    SellerComponent,
    SellerNavComponent,
    SellerDashboardComponent,
    SellerLoginBannerComponent,
    SellerLoginComponent,
    SellerRegisterComponent,
    SellerVerifyComponent,
    AdminApprovalComponent,
    RejectedComponent,
    AddProductComponent,
    DeleteProductComponent,
    SellerChatComponent,
    ViewSellerComponent,
    StartchatComponent,
    CustomerChatComponent,
    AccountComponent,
    AddNewAddressComponent,
    EditAddressComponent,
    CheckoutComponent,
    OrderViewComponent,
    FeedbackComponent,
    EditProductComponent,
    PendingOrdersComponent,
    SetDeliveryDateComponent,
    BannerComponent,
    RatingViewComponent,
    RevenueComponent,
    SearchBoxComponent,
    SellProductComponent,
    AlternateSellerComponent,
    EditAltProductComponent,
    SearchResultComponent,
    ProductByCategoriesComponent,
    CartComponent
  ],
  entryComponents: [
    LoginAndSignupComponent, 
    SellerLoginComponent, 
    SellerRegisterComponent, 
    AddProductComponent,
    DeleteProductComponent,
    SellerChatComponent,
    StartchatComponent,
    CustomerChatComponent,
    AddNewAddressComponent,
    EditAddressComponent,
    CheckoutComponent,
    OrderViewComponent,
    FeedbackComponent,
    EditProductComponent,
    PendingOrdersComponent,
    SetDeliveryDateComponent,
    AlternateSellerComponent,
    EditAltProductComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgOtpInputModule,
    CarouselModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
