import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentication.service';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class BuyproductService {
  baseUrl
  constructor(private auth: AuthenticationService, private http: HttpClient, private api: ApiService) {
    this.baseUrl = api.baseUrl;
   }

   checkoutData(slug, sellerid):Observable<any>{
    let token = window.localStorage.getItem('spiotoken') ? window.localStorage.getItem('spiotoken') : false
    let usr = window.localStorage.getItem('spiouid') ? window.localStorage.getItem('spiouid') : false
    if(token && usr){
      let headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(this.baseUrl+"checkout-data/", {username:usr,product:slug,seller:sellerid}, {headers:headers})
    }
    else{
      let res = {
        success: false,
        message: "please login."
      }
      return of(JSON.stringify(res))
    }
   }

  
}
