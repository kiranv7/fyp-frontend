import { Injectable } from '@angular/core';
import { MatSnackBar, MatDialog } from '@angular/material';
import { AuthenticationService } from '../authentication.service';
import { LoginAndSignupComponent } from 'src/app/dialogs/login-and-signup/login-and-signup.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiService } from '../api.service';
import { of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(public cartMessage: MatSnackBar, private auth: AuthenticationService, private dialog: MatDialog, private http: HttpClient, private api: ApiService) {
    
  }

  addToCart(p_id):Observable<any>{
    let flag = this.auth.getisLoggedin()
   
    if(flag){
      let baseUrl = this.api.baseUrl
      let token = window.localStorage.getItem("spiotoken")
      let username = window.localStorage.getItem("spiouid")

      var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(baseUrl + "add-to-cart/", {username:username, prod_id: p_id},  {headers: headers})

    }else{
      this.dialog.open(LoginAndSignupComponent, {width: '650px'})
      let res = {
        "success":false
      }
      return of(res)
    }
  }

  get cartLength(){
    return window.localStorage.getItem("spioCart") ? parseInt(window.localStorage.getItem("spioCart")) : 0
  }

  sellerRegister(val, val2){
    let baseUrl = this.api.baseUrl
    let data = {...val, ...val2}
    return this.http.post(baseUrl + "seller-register/", data)
  }

  getCartItems(){
    let flag = this.auth.getisLoggedin()
   
    if(flag){
      let baseUrl = this.api.baseUrl
      let token = window.localStorage.getItem("spiotoken")
      let username = window.localStorage.getItem("spiouid")

      var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(baseUrl + "cart/", {username:username},  {headers: headers})

    }else{
      this.dialog.open(LoginAndSignupComponent, {width: '650px'})
      let res = {
        "success":false
      }
      return of(res)
    }
  }

  removeCart(productSlug){
    let flag = this.auth.getisLoggedin()

    if(flag){
      let baseUrl = this.api.baseUrl
      let token = window.localStorage.getItem("spiotoken")
      let username = window.localStorage.getItem("spiouid")

      var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(baseUrl + "remove-cart/", {username:username, product: productSlug},  {headers: headers})

    }else{
      this.dialog.open(LoginAndSignupComponent, {width: '650px'})
      let res = {
        "success":false
      }
      return of(res)
    }
  }


}
