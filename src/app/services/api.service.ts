import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ApiService {
  baseUrl = "http://127.0.0.1:8000/api/"
  

  constructor(private http: HttpClient) { }

  getAllProducts(){
      return this.http.get(this.baseUrl+"products/")
  }

  updateProfile(val): Observable<any>{
    let token = window.localStorage.getItem('spiotoken') ? window.localStorage.getItem('spiotoken') : false
    let usr = window.localStorage.getItem('spiouid') ? window.localStorage.getItem('spiouid') : false
    if(token && usr){
      let headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(this.baseUrl+"update-profile/", {username:usr,data:val}, {headers:headers})
    }
    else{
      let res = {
        success: false,
        message: "please login."
      }
      return of(res)
    }
  }

  getCustomerData(): Observable<any>{
    let token = window.localStorage.getItem('spiotoken') ? window.localStorage.getItem('spiotoken') : false
    let usr = window.localStorage.getItem('spiouid') ? window.localStorage.getItem('spiouid') : false
    if(token && usr){
      var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(this.baseUrl+"get-customer/",{username:usr}, {headers:headers})
    }
    else{
      let res = {
        success: false,
        message: "please login."
      }
      return of(res)
    }
  }


  getBanners():Observable<any> {
    return this.http.get(this.baseUrl + "banners/")
  }

  getProduct(slug){
    return this.http.get(this.baseUrl+"products/"+slug+"/")
  }

  sendToSeller(msg, id){
    let token = window.localStorage.getItem('spiotoken') ? window.localStorage.getItem('spiotoken') : false
    let usr = window.localStorage.getItem('spiouid') ? window.localStorage.getItem('spiouid') : false
    if(token && usr){
      var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(this.baseUrl+"start-chat/",{message:msg, to:id, from:usr}, {headers:headers})
    }
  }

  addAddress(data): Observable<any>{
    let token = window.localStorage.getItem('spiotoken') ? window.localStorage.getItem('spiotoken') : false
    let usr = window.localStorage.getItem('spiouid') ? window.localStorage.getItem('spiouid') : false
    if(token && usr){
      var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(this.baseUrl+"add-address/", {username:usr, data}, {headers:headers})
    }
    else{
      let res = {
        success: false,
        message: "please login."
      }
      return of(res)
    }
  }

  deleteAddress(id):Observable<any>{
    let token = window.localStorage.getItem('spiotoken') ? window.localStorage.getItem('spiotoken') : false
    let usr = window.localStorage.getItem('spiouid') ? window.localStorage.getItem('spiouid') : false
    if(token && usr){
      var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(this.baseUrl+"delete-address/", {username:usr, id:id}, {headers:headers})
    }
    else{
      let res = {
        success: false,
        message: "please login."
      }
      return of(res)
    }
  }

  editAddress(data, id):Observable<any>{
    let token = window.localStorage.getItem('spiotoken') ? window.localStorage.getItem('spiotoken') : false
    let usr = window.localStorage.getItem('spiouid') ? window.localStorage.getItem('spiouid') : false
    if(token && usr){
      var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(this.baseUrl+"edit-address/", {username:usr, data, id:id}, {headers:headers})
    }
    else{
      let res = {
        success: false,
        message: "please login."
      }
      return of(res)
    }
  }

  payment(data):Observable<any>{
    let token = window.localStorage.getItem('spiotoken') ? window.localStorage.getItem('spiotoken') : false
    let usr = window.localStorage.getItem('spiouid') ? window.localStorage.getItem('spiouid') : false
    if(token && usr){
      var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(this.baseUrl+"pay/", {username:usr, data}, {headers:headers})
    }
    else{
      let res = {
        success: false,
        message: "please login."
      }
      return of(res)
    }
  }

  getOrder(orderid){
    let token = window.localStorage.getItem('spiotoken') ? window.localStorage.getItem('spiotoken') : false
    let usr = window.localStorage.getItem('spiouid') ? window.localStorage.getItem('spiouid') : false
    if(token && usr){
      var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(this.baseUrl+"get-order/", {username:usr, orderid: orderid}, {headers:headers})
    }
    else{
      let res = {
        success: false,
        message: "please login."
      }
      return of(res)
    }
  }

  cancelOrder(order){
    let token = window.localStorage.getItem('spiotoken') ? window.localStorage.getItem('spiotoken') : false
    let usr = window.localStorage.getItem('spiouid') ? window.localStorage.getItem('spiouid') : false
    if(token && usr){
      var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(this.baseUrl+"cancel-order/", {username:usr, order: order}, {headers:headers})
    }
    else{
      let res = {
        success: false,
        message: "please login."
      }
      return of(res)
    }
  }


  refundStatus(order): Observable<any>{
    let token = window.localStorage.getItem('spiotoken') ? window.localStorage.getItem('spiotoken') : false
    let usr = window.localStorage.getItem('spiouid') ? window.localStorage.getItem('spiouid') : false
    if(token && usr){
      var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(this.baseUrl+"get-refund-status/", {username:usr, order: order}, {headers:headers})
    }
    else{
      let res = {
        success: false,
        message: "please login."
      }
      return of(res)
    }
  }

  getReviews(slug):Observable<any> {
      return this.http.post(this.baseUrl+"get-reviews/", {product: slug})
  }


  sentFeedback(order, rating, comment): Observable<any>{
    let token = window.localStorage.getItem('spiotoken') ? window.localStorage.getItem('spiotoken') : false
    let usr = window.localStorage.getItem('spiouid') ? window.localStorage.getItem('spiouid') : false
    if(token && usr){
      var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(this.baseUrl+"feedback/", {username:usr, order: order, rating:rating, comment:comment}, {headers:headers})
    }
    else{
      let res = {
        success: false,
        message: "please login."
      }
      return of(res)
    }
  }
  
}
