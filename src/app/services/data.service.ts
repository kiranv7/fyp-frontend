import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  products;

  constructor() { }

  setProducts(val){
    this.products = val
  }

  getProducts(){
    return this.products
  }
}
