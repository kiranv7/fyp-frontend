import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiService } from './api.service';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  httpHeaders;
  username;
  all_orders;
  baseURL;
  token;
  isLoggedin =( window.localStorage.getItem('spiotoken') && window.localStorage.getItem('spiouid') ) ? true : false;

  constructor(private http: HttpClient, private authApi: ApiService) { 
    this.username = window.localStorage.getItem('spioUsername') ? window.localStorage.getItem('spioUsername') : false
    this.token = window.localStorage.getItem('spiotoken') ? window.localStorage.getItem('spiotoken') : false
    //console.log('ser');
    this.baseURL = this.authApi.baseUrl; 
  }

  sellerLogin(data){
    this.clearLocal()
    this.clearSellerLocal()

    return this.http.post(this.baseURL+"seller-login/", {sellerid:data.sellerid, password:data.password})
  }

  setSellerLocal(name,token,id,flag){
    window.localStorage.setItem("sellerName", name)
    window.localStorage.setItem("sellerToken", token)
    window.localStorage.setItem("sellerId", id)
    window.localStorage.setItem("sellerLoggedIn", flag)
  }


  clearSellerLocal(){
    window.localStorage.removeItem("sellerName")
    window.localStorage.removeItem("sellerToken")
    window.localStorage.removeItem("sellerId")
    window.localStorage.removeItem("sellerLoggedIn")
  }

  checkAuth(){
    if(this.isLoggedin){

      this.checkToken().subscribe((data:any)=>{
        if(!data.success){
          this.clearLocal()
        }
      })
    }
  }

  checkToken(){
    let token = window.localStorage.getItem('spiotoken') ? window.localStorage.getItem('spiotoken') : false
    let usr = window.localStorage.getItem('spiouid') ? window.localStorage.getItem('spiouid') : false

    if(token && usr){
      var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(this.baseURL + 'checkToken/', {username: usr}, {headers: headers})
    }
  }

  clearLocal(){
    window.localStorage.removeItem('spiologgedIn')
    window.localStorage.removeItem('spioUsername')
    window.localStorage.removeItem('spiotoken')
    window.localStorage.removeItem('spiouid')
    window.localStorage.removeItem('spioCart')
    this.username = '';
    this.isLoggedin = false;
  }

  orders(){
    let token = window.localStorage.getItem('spiotoken')
    let usr = window.localStorage.getItem('spiouid')
    if(this.isLoggedin && token && usr){
      var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(this.baseURL + 'orders/', {username: usr}, {headers: headers})
    }
  }

  logout(){
    this.httpHeaders  = new HttpHeaders({'Content-type':'application/json'})
    return this.http.post(this.baseURL + 'logout/',{headers:this.httpHeaders})

  }

  login(value){
    this.clearLocal()
    this.clearSellerLocal()

    this.httpHeaders  = new HttpHeaders({'Content-type':'application/json'})
    return this.http.post(this.baseURL + 'login/',{username: value.username, password: value.password},{headers:this.httpHeaders})
  }

  signUp(value){
    this.httpHeaders  = new HttpHeaders({'Content-type':'application/json'})
    console.log(this.httpHeaders);
    
    return this.http.post(this.baseURL + 'signup/',value, {headers:this.httpHeaders})
  }

  getUsername(){
    return window.localStorage.getItem('spioUsername')
  }

  getUserId(){
    return (window.localStorage.getItem('spiouid') ? window.localStorage.getItem('spiouid') : false)
  }

  getToken(){
    return (window.localStorage.getItem('spiotoken') ? window.localStorage.getItem('spiotoken') : false)
  }

  setUsername(name){
    this.username = name
  }

  getisLoggedin(){
    return this.isLoggedin
  }

  setisLoggedin(val){
    this.isLoggedin = val
  }

  setAuth(fname,uname,token,flag,cart){
      window.localStorage.setItem("spiotoken", token)
      window.localStorage.setItem("spioUsername", fname)
      window.localStorage.setItem('spiouid', uname)
      window.localStorage.setItem("spiologgedIn", flag)
      window.localStorage.setItem("spioCart", cart)

      this.username = fname
      this.isLoggedin = flag
  }


}
