import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SellerService {
  baseUrl;

  constructor(private api: ApiService, private http: HttpClient) { 
    this.baseUrl = api.baseUrl
  }

  getSellerInfo(id){
    return this.http.post(this.baseUrl + 'get-seller-info/', {id:id})
  }


  getSellerData():Observable<any>{
    let token = window.localStorage.getItem("sellerToken")
    let sellerid = window.localStorage.getItem("sellerId")

    if(token && sellerid){
      var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
      return this.http.post(this.baseUrl + 'seller-dashboard/', {sellerid: sellerid}, {headers: headers})

    }else{
      let res = {
        "success":false,
        "message":"login first!"
      }
      return of(JSON.stringify(res))
    }
  }

  get getSellerToken(){
    let token = window.localStorage.getItem("sellerToken")
    return token ? token : ''
  }

  get getSellerUsername(){
    let id = window.localStorage.getItem("sellerId")
    return id ? id : ''
  }

  verifyOtp(otp){
    let token = window.localStorage.getItem("sellerToken")
    let username = window.localStorage.getItem("sellerId")

    var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
    return this.http.post(this.baseUrl + "verify-seller/", {username: username, otp: otp}, {headers: headers})
  }

  resendOtp(){
    let token = window.localStorage.getItem("sellerToken")
    let username = window.localStorage.getItem("sellerId")

    var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})
    return this.http.post(this.baseUrl + "resend-seller-otp/", {username: username}, {headers: headers})
  }

  addProduct(form, image){
    let token = window.localStorage.getItem("sellerToken")
    let username = window.localStorage.getItem("sellerId")

    let req:FormData = new FormData()

    for(let key of Object.keys(form)){
      req.append(key, form[key])
    }
    req.append('image',image)
    req.append('username', username)
    req.append('token', token)
    
    console.log(req);
     
    return this.http.post(this.baseUrl + "add-product/", req, {reportProgress:true, observe:'events'}) 
  }

  editProduct(form, image, product){
    let token = window.localStorage.getItem("sellerToken")
    let username = window.localStorage.getItem("sellerId")

    let req:FormData = new FormData()

    for(let key of Object.keys(form)){
      req.append(key, form[key])
    }
    req.append('image',image)
    req.append('username', username)
    req.append('token', token)
    req.append('product', product)
    
    console.log(req);
     
    return this.http.post(this.baseUrl + "edit-product/", req, {reportProgress:true, observe:'events'}) 
  }

  deleteProduct(slug, type){
    let token = window.localStorage.getItem("sellerToken")
    let username = window.localStorage.getItem("sellerId")

    var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":token})

    return this.http.post(this.baseUrl + "delete-product/", {username:username, slug:slug, type:type}, {headers:headers})
  }

  pendingOrders(): Observable<any>{

    let sellerData = this.isSellerLoggedIn()

    if ( sellerData.loggedIn ) {
      var headers  = new HttpHeaders({"Content-type":"application/json","Authorization":sellerData.token})
      return this.http.post(this.baseUrl + "pending-orders/", {username:sellerData.username}, {headers:headers})
    }
    else {

      let res = {
        "success":false,
        "message":"session ended login again!"
      }
      return of(res)

    }
  }



  setExpectedDelivery(day, month, year, status, oid): Observable<any> {
    let seller_data = this.isSellerLoggedIn()

    if( seller_data.loggedIn ) {
      var headers = new HttpHeaders({"Content-type":"application/json","Authorization":seller_data.token})
      return this.http.post(this.baseUrl + "set-expected-delivery/", {username:seller_data.username, day: day, month: month, year: year, status:status, order_id: oid}, {headers:headers})
    }
    else {
      let res = {
        "success":false,
        "message":"login again!"
      }
      return of(res)
    }
  }


  getEarnings():Observable<any> {
    let seller_data = this.isSellerLoggedIn()
    
    if( seller_data.loggedIn ) {
      var headers = new HttpHeaders({"Content-type":"application/json","Authorization":seller_data.token})
      return this.http.post(this.baseUrl + "get-earnings/", {username:seller_data.username}, {headers:headers})
    }
    else {
      let res = {
        "success":false,
        "message":"login again!"
      }
      return of(res)
    }
  }


  altSellerReq(stock, price, slug): Observable<any> {
    let seller_data = this.isSellerLoggedIn()
    if( seller_data.loggedIn ) {
      var headers = new HttpHeaders({"Content-type":"application/json","Authorization":seller_data.token})
      return this.http.post(this.baseUrl + "alternate-seller-request/", {username:seller_data.username, selling_price: price, stock: stock, product: slug}, {headers:headers})
    }
    else {
      let res = {
        "success":false,
        "message":"login again!"
      }
      return of(res)
    }
  }


  getAltInfo(id): Observable<any> {
    let seller_data = this.isSellerLoggedIn()
    if( seller_data.loggedIn ) {
      var headers = new HttpHeaders({"Content-type":"application/json","Authorization":seller_data.token})
      return this.http.post(this.baseUrl + "get-alt/", {username:seller_data.username, id: id}, {headers:headers})
    }
    else {
      let res = {
        "success":false,
        "message":"login again!"
      }
      return of(res)
    }
  }


  setAlt(id, price, stock): Observable<any> {
    let seller_data = this.isSellerLoggedIn()
    if( seller_data.loggedIn ) {
      var headers = new HttpHeaders({"Content-type":"application/json","Authorization":seller_data.token})
      return this.http.post(this.baseUrl + "set-alt/", {username:seller_data.username, id: id, price:price, stock:stock}, {headers:headers})
    }
    else {
      let res = {
        "success":false,
        "message":"login again!"
      }
      return of(res)
    }
  }



  isSellerLoggedIn(): any {
    let token = window.localStorage.getItem('sellerToken') ? window.localStorage.getItem('sellerToken') : false
    let username = window.localStorage.getItem('sellerId') ? window.localStorage.getItem('sellerId') : false

    if ( token && username ) {
      let meta = {
        loggedIn: true,
        token: token,
        username: username
      }
      return meta
    }
    else {
      let meta = {
        loggedIn: false
      }
      return meta
    }
  }

  
}
