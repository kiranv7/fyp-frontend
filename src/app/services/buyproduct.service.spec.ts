import { TestBed } from '@angular/core/testing';

import { BuyproductService } from './buyproduct.service';

describe('BuyproductService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BuyproductService = TestBed.get(BuyproductService);
    expect(service).toBeTruthy();
  });
});
