import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements  CanActivate{
  constructor(private auth: AuthenticationService, private router: Router){

  }

  debugger
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
    ): Observable<boolean>|Promise <boolean> | boolean{
      if(!this.auth.getisLoggedin()){
        this.router.navigate(['']);
      }
      return this.auth.getisLoggedin();
    }
}
