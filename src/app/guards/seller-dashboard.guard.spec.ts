import { TestBed, async, inject } from '@angular/core/testing';

import { SellerDashboardGuard } from './seller-dashboard.guard';

describe('SellerDashboardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SellerDashboardGuard]
    });
  });

  it('should ...', inject([SellerDashboardGuard], (guard: SellerDashboardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
