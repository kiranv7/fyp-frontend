import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class SellerDashboardGuard implements CanActivate {
  constructor(private router: Router){}

  debugger
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
    ): Observable<boolean>|Promise <boolean> | boolean{
      let loggedIn = window.localStorage.getItem('sellerLoggedIn') ? true : false;

      if(loggedIn){
        return true;
      }
      else{
        this.router.navigate(['/seller'])
        return false;
      }
    }
}
